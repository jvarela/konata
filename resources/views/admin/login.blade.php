
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Konata - {{ _('Acceder') }}</title>
    
    <link rel="shortcut icon" href="/backend/img/favicon.png" />
    {!! HTML::style('backend/css/bootstrap.min.css') !!}
    {!! HTML::style('backend/css/metisMenu.min.css') !!}
    {!! HTML::style('backend/css/admin.css') !!}
    {!! HTML::style('backend/css/font-awesome.min.css') !!}
    {!! HTML::style('backend/css/konata.css') !!}

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="admin login">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-logo">
                    {!! HTML::image('backend/img/konata.png', 'Logo de Konata') !!}
                </div>
                <div class="login-panel panel panel-default">
                    <div class="panel-body">
                        <form role="form" action="{{ route('admin.login') }}" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <label>{{ _('Nombre de usuario o Email') }}</label>
                                    <input class="form-control" name="username" type="text" autofocus value="{{ old('username') }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ _('Contraseña') }}</label>
                                    <input class="form-control" name="password" type="password">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="remember">{{ _('Recuérdame') }}
                                    </label>
                                </div>
                                <input type="submit" class="btn btn-lg btn-primary btn-block" value="Acceder" />
                            </fieldset>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
                <a href="{{ route('index') }}">&laquo; {{ _('Volver al sitio web') }}</a>
            </div>
        </div>
    </div>

    {!! HTML::script('backend/js/jquery.min.js') !!}
    {!! HTML::script('backend/js/bootstrap.min.js') !!}
    {!! HTML::script('backend/js/metisMenu.min.js') !!}
    {!! HTML::script('backend/js/admin.js') !!}

</body>
</html>

