<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class UsersController extends Controller
{
    /**
     * Muestra un listado de los usuarios registrados
     */
    public function index()
    {
        return 'listado';
    }
    
    /**
     * Muestra el formulario para la creación de un usuario
     */
    public function create()
    {
        return 'añadiendo';
    }
    
    /**
     * Guardar un usuario en la db
     * 
     * @param $request Request Objeto Request con el formulario parseado
     */
    public function store(Request $request)
    {
        return 'guardando';
    }
    
    /**
     * Muestra el formulario para editar un usuario en particular
     * 
     * @param $id Integer ID del usuario a editar
     */
    public function edit($id = null)
    {
        if (!$id) {
            Session::flash('errors', 'No se ha proporcionado ningún usuario que editar.');
            return redirect()->route('admin::users');
        }
        
        return 'editando usuario ' . $id;
    }
    
    /**
     * Guarda un usuario en la db
     * 
     * @param $request Request Objeto Request con el formulario parseado 
     */
    public function update(Request $request)
    {
        return 'guardando usuario';
    }
    
    /**
     * Borra un usuario de la db
     * 
     * @param $id Integer ID del usuario a borrar
     */
    public function destroy($id = null)
    {
        if (!$id) {
            Session::flash('errors', 'No se ha proporcionado ningún usuario que borrar.');
            return redirect()->route('admin::users');
        }
        
        return 'borrando usuario ' . $id;
    }
}
