
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Konata - <?php echo e(_('Acceder')); ?></title>
    
    <link rel="shortcut icon" href="/admin/img/favicon.png" />
    <?php echo HTML::style('admin/css/bootstrap.min.css'); ?>

    <?php echo HTML::style('admin/css/metisMenu.min.css'); ?>

    <?php echo HTML::style('admin/css/admin.css'); ?>

    <?php echo HTML::style('admin/css/font-awesome.min.css'); ?>

    <?php echo HTML::style('admin/css/konata.css'); ?>


    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="admin login">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-logo">
                    <?php echo HTML::image('admin/img/konata.png', 'Logo de Konata'); ?>

                </div>
                <div class="login-panel panel panel-default">
                    <div class="panel-body">
                        <form role="form" action="<?php echo e(route('admin::login')); ?>" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <label><?php echo e(_('Nombre de usuario o Email')); ?></label>
                                    <input class="form-control" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <label><?php echo e(_('Contraseña')); ?></label>
                                    <input class="form-control" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="remember"><?php echo e(_('Recuérdame')); ?>

                                    </label>
                                </div>
                                <input type="submit" class="btn btn-lg btn-primary btn-block" value="Acceder" />
                            </fieldset>
                            <?php echo e(csrf_field()); ?>

                        </form>
                    </div>
                </div>
                <a href="<?php echo e(route('index')); ?>">&laquo; <?php echo e(_('Volver al sitio web')); ?></a>
            </div>
        </div>
    </div>

    <?php echo HTML::script('admin/js/jquery.min.js'); ?>

    <?php echo HTML::script('admin/js/bootstrap.min.js'); ?>

    <?php echo HTML::script('admin/js/metisMenu.min.js'); ?>

    <?php echo HTML::script('admin/js/admin.js'); ?>


</body>
</html>

