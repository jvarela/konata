<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Paginas y posts de frontend
Route::any('{slug?}', ['as' => 'index', 'uses' => 'Frontend\NodeController@index'])->where('slug', '^(?!admin).*$');

// Panel de administracion
Route::group(['prefix' => 'admin', 'namespace' => 'admin'], function()
{
    // Listado y acciones sobre usuarios
    Route::resource('users', 'UsersController');
    
    // Paginas individuales
    Route::controller('', 'AdminController', [
        'getIndex' => 'admin',
        'getLogin' => 'admin.login'
    ]);
});