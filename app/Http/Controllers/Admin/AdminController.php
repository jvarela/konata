<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getLogin', 'postLogin']]);
        $this->middleware('admin.guest', ['only' => ['getLogin', 'postLogin']]);
    }
    public function getIndex()
    {
        return 'admin';
    }
    
    public function getLogin()
    {
        return view('admin.login');
    }
    
    public function postLogin(Request $request)
    {
        return Input::all();
    }
}